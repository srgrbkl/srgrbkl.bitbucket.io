// jQuery no conflict
jQuery.noConflict();

jQuery(document).ready(function (jQuery) {

    // touch
    jQuery(function () {
        var $html = jQuery('html');
        if ("ontouchstart" in window || "ontouch" in window) {
            $html.addClass('touch');
        } else {
            $html.addClass('no-touch');
        }
    });


    // carousel
    jQuery(function () {
        if (jQuery(".carousel").length) {
            var $carousel = jQuery(".carousel");
            jQuery(window).on("load resize", function () {
                if (jQuery(window).width() < 480) {
                    $carousel.carousel('init');
                } else {
                    $carousel.carousel('destroy');
                }
            });

        }
    });

});

