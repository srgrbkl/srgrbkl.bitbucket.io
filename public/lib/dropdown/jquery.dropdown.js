+function ($) {
    'use strict';

    var trigger = {},
        triggerClass = "",
        triggerAttr = "",
        jqDropdown = {},
        isOpen = false;

    // show
    function show(event, object) {

        trigger = event ? jQuery(this) : object;
        triggerAttr = trigger.attr('data-jq-dropdown');
        jqDropdown = jQuery("#" + triggerAttr);
        isOpen = trigger.hasClass('jq-dropdown-active');


        if (event) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            if (trigger !== object.target) return;
        }

        hide();

        if (isOpen) return;

        triggerClass = triggerAttr + "-active";

        jQuery("[data-jq-dropdown ='" + triggerAttr + "']").addClass('jq-dropdown-active');
        jqDropdown.addClass('jq-dropdown-open');

    }

    // hide
    function hide(event) {

        jQuery(document).find('.jq-dropdown-active').removeClass('jq-dropdown-active');
        jQuery(document).find('.jq-dropdown-open').removeClass('jq-dropdown-open');

    }


    if (jQuery("[data-jq-dropdown]").length) {

        jQuery(document).on('click', '[data-jq-dropdown]', show);
        jQuery(document).on('click', '.jq-dropdown-panel', function (e) {
            e.stopPropagation()
        });
        jQuery(document).on('click', hide);

    }

}(jQuery);