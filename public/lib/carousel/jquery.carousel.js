+(function ($) {
    'use strict';

    var flag = true,
        methods = {

            init: function (options) {
                var set = $.extend({
                    duration: 300,
                    nextClass: 'carousel-next',
                    prevClass: 'carousel-prev'
                }, options);

                return this.each(function () {

                    var $this = $(this),
                        data = $this.data('carousel'),
                        carousel = $this.addClass("initialized");


                    if (!data) {

                        var $carousel = carousel,
                            //components
                            $next = $carousel.find('.' + set.nextClass + ''),
                            $prev = $carousel.find('.' + set.prevClass + ''),
                            $carouselList = $carousel.children().not($prev).not($next),
                            $track = $carouselList.children(),
                            $items = $track.children(),
                            n = $items.length,
                            ratio = $carousel.height() / $carousel.width(),
                            w = 0,
                            h = 0;

                        $track.children().last().prependTo($track);

                        //dimensions
                        var sliderSize = function () {
                            w = $carousel.width();
                            $items.width(w);
                            $track.width(n * w);
                            $track.css('left', -w);
                        };

                        sliderSize();
                        flag = true;
                        $(window).resize(function () {
                            if(flag){
                                sliderSize();
                            }
                        });

                        //moving slides
                        var next = function () {
                            $track.animate({
                                'margin-left': '-=' + w
                            }, set.duration, function () {
                                var $this = $(this);
                                $this.children().first().appendTo($this);
                                $this.css('margin-left', 0);
                            });
                        };

                        var prev = function () {
                            $track.animate({
                                'margin-left': '+=' + w
                            }, set.duration, function () {
                                var $this = $(this);
                                $this.children().last().prependTo($this);
                                $this.css('margin-left', 0);
                            });
                        };

                        //slide navigation
                        $next.on('click', next);
                        $prev.on('click', prev);

                        $(this).data('carousel', {
                            target: $this
                        });

                    }

                });

            },
            destroy: function () {

                return this.each(function () {
                    var $this = $(this),
                        data = $this.data('carousel'),
                        $next = $this.find('.carousel-next'),
                        $prev = $this.find('.carousel-prev'),
                        $carouselList = $(this).children().not($prev).not($next),
                        $track = $carouselList.children(),
                        $items = $track.children();

                    // reset
                    flag = false;
                    $this.removeData('carousel');
                    $this.removeClass('initialized');
                    $next.unbind("click");
                    $prev.unbind("click");
                    $track.removeAttr('style');
                    $items.removeAttr('style');

                })
            }

    };

    $.fn.carousel = function (methodOrOptions) {
        if (methods[methodOrOptions]) {
            return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            // Default to "init"
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.carousel');
        }
    };

}(jQuery));